# Censys Scanner

#### Created by Faelin Landy as a demonstration for Censys.io
The scripts contained in this repository are derived in part
from code originally written by [Alexander Ravikovich](https://github.com/spaiz/go-mysql-proxy),
as part of a tutorial on MySQL Proxy Servers.

## Intro

This project uses Go scripting to scan a list of addresses
in the form of "host:port" by attempting to initiate a TCP
connection to each port.

The response to each TCP connection attempt is inspected,
with the goal of determining if the targeted address is
bound to a MySQL service running on the remote host.

There are three ways to use the service:

## Test Mode
In this mode a testbed of containerized, networked services
is generated and linked into a shared Docker network.

Next, a list of the exposed host:port addresses is generated
and stored in the `Scanner/addresses` file.

Finally, the Scanner program is spun up within another
networked Docker container, and will begin automatically
scanning the listed addresses.

To use Test mode
```
$> CensysScanner/run init test
```
#### Please note that you will need Docker installed locally to run this project in Test mode.

## Manual Mode
In this mode a single container is spun up, containing a
Golang execution environment (`golang:latest`).

As with the Test mode, this container will immediately begin
scanning destinations from the `addresses` file.

However, *unlike* Test mode, you will need to manually add
desired destinations to the `addresses` file in the form of
'host:port', like so:
```
192.0.2.0:8080
192.0.2.0:3306
127.0.0.1:4000
```

To use Manual mode
```
$> CensysScanner/run manual
```
#### Please be sure to populate the `addresses` prior to running starting Manual mode

## Local Mode
This mode is similar to Manual mode, however the Scanner
will be run using your local Go installation.

To use Local mode
```
$> CensysScanner/run local
```
#### Please be sure to populate the `addresses` prior to running starting Local mode

## Debug Mode
This Scanner project provides access to a remote debugging
mode, intended to be used with local IDE or debugging
consoles.

The Debug mode will create a special containerized Go
environment running the Delve remote Go debug server,
which is exposed to the localhost on port 40000.

Please consult the [Delve documentation](https://github.com/go-delve/delve)
for more information on how to connect to a Delve server.

To use Debug mode
```
$> CensysScanner/run init generate_addresses   # if you want to use the Test network
$> CensysScanner/run debug
```
