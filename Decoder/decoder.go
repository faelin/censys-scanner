package Decoder

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
)

type PacketHeader struct {
	Length     uint32
	SequenceId uint8
}

/*
HandshakePacket represents an initial handshake packet sent by a MySQL Server
*/
type HandshakePacket struct {
	ProtocolVersion   uint8
	ServerVersion     []byte
	ConnectionId      uint32
	AuthPluginData    []byte
	Filler            byte
	CapabilitiesFlag  CapabilitiesFlag
	CharacterSet      uint8
	StatusFlags       StatusFlag
	AuthPluginDataLen uint8
	AuthPluginName    []byte
	Header            *PacketHeader
}

func (hs *HandshakePacket) MarshalJSON() ([]byte, error) {
	data := map[string]interface{}{
		"ProtocolVersion":   strconv.Itoa(int(hs.ProtocolVersion)),
		"ServerVersion":     string(hs.ServerVersion),
		"ConnectionId":      strconv.Itoa(int(hs.ConnectionId)),
		"AuthPluginData":    string(hs.AuthPluginData),
		"CapabilitiesFlag":  hs.CapabilitiesFlag.Marshal(),
		"CharacterSet":      strconv.Itoa(int(hs.CharacterSet)),
		"StatusFlags":       hs.StatusFlags.Marshal(),
		"AuthPluginDataLen": strconv.Itoa(int(hs.AuthPluginDataLen)),
		"AuthPluginName":    string(hs.AuthPluginName),
	}
	return json.Marshal(data)
}

// DecodeHandshake decodes the handshake packet received from the MySQl Server
func DecodeHandshake(data []byte) (*HandshakePacket, error) {
	handshake := &HandshakePacket{}

	header := &PacketHeader{}
	ln := []byte{data[0], data[1], data[2], 0x00}
	header.Length = binary.LittleEndian.Uint32(ln)
	// a single byte integer is the same in BigEndian and LittleEndian
	header.SequenceId = data[3]
	handshake.Header = header

	/**
	Assign payload only data to new var just for convenience
	*/
	payload := data[4 : header.Length+4]
	position := 0

	// protocol_version (1 byte)
	handshake.ProtocolVersion = payload[0]
	position += 1

	// extract server version by searching for the terminal character (0x00)
	// server_version (variable length)
	end := bytes.IndexByte(payload[position:], byte(0x00))
	handshake.ServerVersion = payload[position : position+end]
	position += end + 1

	// connection_id (4 bytes)
	connectionId := payload[position : position+4]
	id := binary.LittleEndian.Uint32(connectionId)
	handshake.ConnectionId = id
	position += 4

	// auth-plugin-data is concatenated from two different chunks of the handshake
	// auth_plugin_data_part_1 (string.fix_len  --  8 bytes)
	handshake.AuthPluginData = make([]byte, 8)
	copy(handshake.AuthPluginData, payload[position:position+8])
	position += 8

	// empty value
	// filler_1 (1 byte)
	handshake.Filler = payload[position]
	if handshake.Filler != 0x00 {
		log.Fatal("error in decoding, invalid Filler value found: " + fmt.Sprintf("%02x", handshake.Filler))
	}
	position += 1

	// lower 2 bytes of the Protocol::CapabilitiesFlag data
	// capability_flag_1 (2 bytes)
	capabilitiesFlag1 := payload[position : position+2]
	position += 2

	// character_set (1 byte)
	handshake.CharacterSet = payload[position]
	position += 1

	// status_flags (2 bytes)
	handshake.StatusFlags = StatusFlag(binary.LittleEndian.Uint16(payload[position : position+2]))
	position += 2

	// upper 2 bytes of the Protocol::CapabilitiesFlag data
	// capability_flag_2 (2 bytes)
	capabilitiesFlag2 := payload[position : position+2]
	position += 2

	// sum upper two bytes and lower two bytes into a single 32 bit number
	capabilitiesLow := binary.LittleEndian.Uint16(capabilitiesFlag1)
	capabilitiesHigh := binary.LittleEndian.Uint16(capabilitiesFlag2)
	handshake.CapabilitiesFlag = CapabilitiesFlag(uint32(capabilitiesLow) | uint32(capabilitiesHigh)<<16)

	// auth_plugin_data_length (1 byte)
	handshake.AuthPluginDataLen = payload[position]
	position += 1

	// if the CLIENT_PLUGIN_AUTH flag is set, then we need to confirm a valid length indicator
	if handshake.CapabilitiesFlag&clientPluginAuth != 0 && handshake.AuthPluginDataLen == 0 {
		log.Fatal("error in decoding, could not determine auth plugin data length!")
	}

	// reserved empty space (10 bytes)
	position += 10

	// if CLIENT_SECURE_CONNECTION flag is set, we combine auth-plugin-data-part-1 + auth-plugin-data-part-2
	// auth_plugin_data remainder (string.variable-len  --  variable)
	if handshake.CapabilitiesFlag&clientSecureConnection != 0 {
		end := position + max(13, int(handshake.AuthPluginDataLen)-8) // from mysql protocol doc
		handshake.AuthPluginData = append(handshake.AuthPluginData, payload[position:end]...)
		position = end
	}

	// if the CLIENT_PLUGIN_AUTH flag is set, then the remainder of the handshake contains auth_plugin_name
	if handshake.CapabilitiesFlag&clientPluginAuth != 0 {

		// search for string terminal character (0x00)
		end = bytes.IndexByte(payload[position:], byte(0x00))
	}

	/*
		Due to Bug#59453 the auth-plugin-name is missing the terminating NUL-char in versions prior to 5.5.10 and 5.6.2.
		We know the length of the payload, so if there is no NUL-char, just read all the data until the end
	*/
	// auth_plugin_name (variable length)
	if end != -1 {
		handshake.AuthPluginName = payload[position : position+end]
	} else {
		handshake.AuthPluginName = payload[position:]
	}

	return handshake, nil
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

type CapabilitiesFlag uint32 // https://dev.mysql.com/doc/internals/en/capability-flags.html
type StatusFlag uint16       // https://dev.mysql.com/doc/internals/en/status-flags.html

func (cb *CapabilitiesFlag) Marshal() map[string]interface{} {
	data := map[string]interface{}{
		"Value": fmt.Sprintf("0x%08x", *cb),
		"Flags": []string{},
	}

	for i := uint64(1); i <= uint64(1)<<31; i = i << 1 {
		if *cb&CapabilitiesFlag(i) != 0 {
			flag, ok := capabilityFlags[CapabilitiesFlag(i)]
			if ok {
				data["Flags"] = append(data["Flags"].([]string), flag)
			}
		}
	}

	return data
}

func (sb *StatusFlag) Marshal() map[string]interface{} {
	data := map[string]interface{}{
		"Value": fmt.Sprintf("0x%04x", *sb),
		"Flags": []string{},
	}

	for i := uint32(1); i <= uint32(1)<<15; i = i << 1 {
		if *sb&StatusFlag(i) != 0 {
			flag, ok := statusFlags[StatusFlag(i)]
			if ok {
				data["Flags"] = append(data["Flags"].([]string), flag)
			}
		}
	}

	return data
}

const (
	clientLongPassword CapabilitiesFlag = 1 << iota
	clientFoundRows
	clientLongFlag
	clientConnectWithDB
	clientNoSchema
	clientCompress
	clientODBC
	clientLocalFiles
	clientIgnoreSpace
	clientProtocol41
	clientInteractive
	clientSSL
	clientIgnoreSIGPIPE
	clientTransactions
	clientReserved
	clientSecureConnection
	clientMultiStatements
	clientMultiResults
	clientPSMultiResults
	clientPluginAuth
	clientConnectAttrs
	clientPluginAuthLenEncClientData
	clientCanHandleExpiredPasswords
	clientSessionTrack
	clientDeprecateEOF
)

var capabilityFlags = map[CapabilitiesFlag]string{
	clientLongPassword:               "CLIENT_LONG_PASSWORD",
	clientFoundRows:                  "CLIENT_FOUND_ROWS",
	clientLongFlag:                   "CLIENT_LONG_FLAG",
	clientConnectWithDB:              "CLIENT_CONNECT_WITH_DB",
	clientNoSchema:                   "CLIENT_NO_SCHEMA",
	clientCompress:                   "CLIENT_COMPRESS",
	clientODBC:                       "CLIENT_ODBC",
	clientLocalFiles:                 "CLIENT_LOCAL_FILES",
	clientIgnoreSpace:                "CLIENT_IGNORE_SPACE",
	clientProtocol41:                 "CLIENT_PROTOCOL_41",
	clientInteractive:                "CLIENT_INTERACTIVE",
	clientSSL:                        "CLIENT_SSL",
	clientIgnoreSIGPIPE:              "CLIENT_IGNORE_SIGPIPE",
	clientTransactions:               "CLIENT_TRANSACTIONS",
	clientReserved:                   "CLIENT_RESERVED",
	clientSecureConnection:           "CLIENT_SECURE_CONNECTION",
	clientMultiStatements:            "CLIENT_MULTI_STATEMENTS",
	clientMultiResults:               "CLIENT_MULTI_RESULTS",
	clientPSMultiResults:             "CLIENT_PS_MULTI_RESULTS",
	clientPluginAuth:                 "CLIENT_PLUGIN_AUTH",
	clientConnectAttrs:               "CLIENT_CONNECT_ATTRS",
	clientPluginAuthLenEncClientData: "CLIENT_PLUGIN_AUTH_LENENC_CLIENT_DATA",
	clientCanHandleExpiredPasswords:  "CLIENT_CAN_HANDLE_EXPIRED_PASSWORDS",
	clientSessionTrack:               "CLIENT_SESSION_TRACK",
	clientDeprecateEOF:               "CLIENT_DEPRECATE_EOF",
}

const (
	serverStatusInTrans StatusFlag = 1 << iota
	serverStatusAutocommit
	_
	serverMoreResultsExists
	serverStatusNoGoodIndexUsed
	serverStatusNoIndexUsed
	serverStatusCursorExists
	serverStatusLastRowSent
	serverStatusDbDropped
	serverStatusNoBackslashEscapes
	serverStatusMetadataChanged
	serverQueryWasSlow
	serverPsOutParams
	serverStatusInTransReadonly
	serverSessionStateChanged
)

var statusFlags = map[StatusFlag]string{
	serverStatusInTrans:            "SERVER_STATUS_IN_TRANS",
	serverStatusAutocommit:         "SERVER_STATUS_AUTOCOMMIT",
	serverMoreResultsExists:        "SERVER_MORE_RESULTS_EXISTS",
	serverStatusNoGoodIndexUsed:    "SERVER_STATUS_NO_GOOD_INDEX_USED",
	serverStatusNoIndexUsed:        "SERVER_STATUS_NO_INDEX_USED",
	serverStatusCursorExists:       "SERVER_STATUS_CURSOR_EXISTS",
	serverStatusLastRowSent:        "SERVER_STATUS_LAST_ROW_SENT",
	serverStatusDbDropped:          "SERVER_STATUS_DB_DROPPED",
	serverStatusNoBackslashEscapes: "SERVER_STATUS_NO_BACKSLASH_ESCAPES",
	serverStatusMetadataChanged:    "SERVER_STATUS_METADATA_CHANGED",
	serverQueryWasSlow:             "SERVER_QUERY_WAS_SLOW",
	serverPsOutParams:              "SERVER_PS_OUT_PARAMS",
	serverStatusInTransReadonly:    "SERVER_STATUS_IN_TRANS_READONLY",
	serverSessionStateChanged:      "SERVER_SESSION_STATE_CHANGED",
}
