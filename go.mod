module censys-scanner

go 1.15

require (
	github.com/ToQoz/gopwt v2.1.0+incompatible // indirect
	github.com/bmuschko/go-testing-frameworks v0.0.0-20181219150736-701f503b1e78
	github.com/franela/goblin v0.0.0-20201006155558-6240afcb2eb7 // indirect
	github.com/go-check/check v0.0.0-20200902074654-038fdea0a05b // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/maxatome/go-testdeep v1.7.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.3 // indirect
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.6.1 // indirect
)
