package main

import (
	"bufio"
	"censys-scanner/Decoder"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"path/filepath"
	"time"
)

func main() {

	// read the files found in ./addresses to generate a list of IP addresses in the form of ip:port
	path, _ := os.Getwd()
	file, err := os.Open(filepath.Join(path, "addresses"))
	if err != nil {
		log.Fatal(err)
	}

	var addresses []string
	ioScanner := bufio.NewScanner(file)
	for ioScanner.Scan() {
		line := fmt.Sprint(ioScanner.Text())
		addresses = append(addresses, line)
	}
	if err := ioScanner.Err(); err != nil {
		file.Close()
		log.Fatal(err)
	}
	file.Close()

	// attempt to probe the listed addresses
	for _, addr := range addresses {
		fmt.Println("\n\nTesting address " + addr)
		decodedHandshake, err := Probe(addr)
		if err != nil {
			fmt.Printf("Server [%s] is either non-responsive or not an MySQL server:\n\t %s \n", addr, err.Error())
			continue
		} else {
			fmt.Println("\tserver appears to be a MySQL server!")
			jsonString, err := json.MarshalIndent(decodedHandshake, "", "\t")
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println("\nMySQL server report:\n" + string(jsonString))
			}
		}
	}
}

func Probe(address string) (*Decoder.HandshakePacket, error) {
	timeout, _ := time.ParseDuration("5s")
	fmt.Print("\tdialing host (5s timeout)...")
	conn, err := net.DialTimeout("tcp", address, timeout)
	// conn, err := net.Dial("tcp", address)
	if err != nil {
		fmt.Println("failed to connect! " + err.Error())
		return nil, err
	}
	fmt.Println("connection established!")

	fmt.Print("\tinitiating handshake (5s timeout)...")
	timeout, _ = time.ParseDuration("5s")
	_ = conn.SetReadDeadline(time.Now().Add(timeout))
	data := make([]byte, 1024)
	size, err := conn.Read(data)
	if err != nil {
		fmt.Println("could not read from TCP stream!")
		return nil, err
	}
	conn.Close()
	packet := data[:size]

	fmt.Printf("response received (%d bytes):\n", size)
	for i := 0; i <= size / 16; i++ {
		rowStart := i * 16
		chunk1 := printBytes(packet[rowStart : rowStart + 8])

		rowStart += 9
		chunk2 := printBytes(packet[rowStart : rowStart + 8])

		fmt.Printf("\t\t%s  %s\n", chunk1, chunk2)
	}

	fmt.Print("\tattempting to process handshake...")
	decodedHandshake, err := Decoder.DecodeHandshake(packet)
	if err != nil {
		fmt.Println("failed to decode!")
		return nil, err
	}
	fmt.Println("successfully decoded!")

	return decodedHandshake, err
}

func printBytes(chunk []byte) string {
	var output string
	for _, x := range chunk {
		output += fmt.Sprintf("%02x ", x)
	}
	return output
}
